# Javadoc Introduction

A LaTeX document to teach Javadoc to EPFL student of BA1.

If you find some type, please proceed to make a merge request by modifying the file in the language in question.

As of now, I am in no mood to translate it in any other language, you can do this yourself and propose a merge request with a new file name ```TutoJavadoc_[LanguageID].latex```, and I will probably accept it. If you could at the same time provide the PDF version (by putting it in the ```pdf```, it would be great.)

## Overleaf access
To view it in Overleaf and copy it from there, you can use the following link : https://www.overleaf.com/read/ymqjczfvwrbf.
